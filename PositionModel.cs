﻿using System;

namespace LifeGameServer
{
    /// <summary>
    /// Модель записи позиции в БД
    /// </summary>
    public class PositionModel
    {
        public int Id { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public bool[] GameField { get; set; }
        public DateTime SaveTime { get; set; }
    }
}
