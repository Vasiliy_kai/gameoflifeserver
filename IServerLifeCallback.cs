﻿using System.ServiceModel;

namespace LifeGameServer
{
    interface IServerLifeCallback
    {
        [OperationContract(IsOneWay = true)]
        void MsgCallback(string msg);
    }
}
