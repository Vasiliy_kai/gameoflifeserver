﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace LifeGameServer
{
    /// <summary>
    /// Интерфейс обращения к серверу
    /// </summary>
    [ServiceContract(CallbackContract = typeof(IServerLifeCallback))]
    public interface IServiceLife
    {
        /// <summary>
        /// Создает новую игру на основе заданных параметров
        /// </summary>
        /// <param name="field">Игровое поле bool[]</param>
        /// <param name="height">Высота поля</param>
        /// <param name="width">Ширина поля</param>
        /// <param name="endless">Признак бесконечности</param>
        [OperationContract]
        void NewGame(bool[] field, int height, int width, bool endless = false);

        /// <summary>
        /// Начинает следующий ход
        /// </summary>
        /// <param name="twoTurnsCheck">Признак проверки условий окончания игры на два хода назад</param>
        /// <returns></returns>
        [OperationContract]
        int NextTurn(bool twoTurnsCheck);

        /// <summary>
        /// Выводит игровое поле в формате bool[]
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        bool[] Display();

        /// <summary>
        /// Сохраняет текущую игру в БД
        /// </summary>
        /// <param name="startField">Начальное поле</param>
        /// <param name="field">Текущее поле</param>
        /// <param name="height">Высота</param>
        /// <param name="width">Ширина</param>
        /// <param name="gameStart">Время начала игры</param>
        /// <param name="gameEnd">Время окончания игры</param>
        [OperationContract]
        void GameSave(bool[] startField, bool[] field, int height, int width, DateTime gameStart, DateTime gameEnd);

        /// <summary>
        /// Выгружает список сохраненных игр из БД
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<GameModel> GameLoad();

        /// <summary>
        /// Удаляет игру из БД
        /// </summary>
        /// <param name="id">Идентификатор игры</param>
        [OperationContract]
        void GameDelete(int id);

        /// <summary>
        /// Сохраняет текущую позицию в БД
        /// </summary>
        /// <param name="field">Поле</param>
        /// <param name="height">Высота</param>
        /// <param name="width">Ширина</param>
        /// <returns></returns>
        [OperationContract]
        int PositionSave(bool[] field, int height, int width);

        /// <summary>
        /// Выгружает список сохраненных позиций из БД
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<PositionModel> PositionLoad();

        /// <summary>
        /// Удаляет позицию из БД
        /// </summary>
        /// <param name="id">Идентификатор позиции</param>
        [OperationContract]
        void PositionDelete(int id);
    }
}
