﻿using LifeGameEngine;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.ServiceModel;
using System.Text;

namespace LifeGameServer
{
    /// <summary>
    /// Реализует поведение сервера
    /// </summary>
    [ServiceBehavior]
    public class ServiceLife : IServiceLife
    {
        /// <summary>
        /// Игровое поле
        /// </summary>
        internal Board Board { get; set; }

        /// <summary>
        /// Строка для подключения к БД
        /// </summary>
        private string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\Projects\CSharp\LifeGameServer\LifeGameServer\GameOfLifeDB.mdf;Integrated Security=True;";

        /// <summary>
        /// Создает новую игру на основе заданных параметров
        /// </summary>
        /// <param name="field">Игровое поле bool[]</param>
        /// <param name="height">Высота поля</param>
        /// <param name="width">Ширина поля</param>
        /// <param name="endless">Признак бесконечности</param>
        public void NewGame(bool[] field, int height, int width, bool endless = false)
        {
            bool[,] board = new bool[height, width];

            int k = 0;
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    board[i, j] = field[k];
                    k++;
                }
            }
            Board = Board.NewInstance(board, endless);
        }

        /// <summary>
        /// Начинает следующий ход
        /// </summary>
        /// <param name="twoTurnsCheck">Признак проверки условий окончания игры на два хода назад</param>
        /// <returns></returns>
        public int NextTurn(bool twoTurnsCheck)
        {
            return Board.NextTurn(twoTurnsCheck);
        }

        /// <summary>
        /// Выводит игровое поле в формате bool[]
        /// </summary>
        /// <returns></returns>
        public bool[] Display()
        {
            bool [,] field = Board.Display();
            bool[] board = new bool[field.GetLength(0) * field.GetLength(1)];
            int k = 0;
            for (int i = 0; i < field.GetLength(0); i++)
            {
                for(int j = 0; j < field.GetLongLength(1); j++)
                {
                    board[k] = field[i, j];
                    k++;
                }
            }
            return board;
        }

        /// <summary>
        /// Сохраняет текущую игру в БД
        /// </summary>
        /// <param name="startField">Начальное поле</param>
        /// <param name="field">Текущее поле</param>
        /// <param name="height">Высота</param>
        /// <param name="width">Ширина</param>
        /// <param name="gameStart">Время начала игры</param>
        /// <param name="gameEnd">Время окончания игры</param>
        public void GameSave(bool[] startField, bool[] field, int height, int width, DateTime gameStart, DateTime gameEnd)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                StringBuilder sbField = new StringBuilder();
                StringBuilder sbStartField = new StringBuilder();

                for (int i = 0; i < field.GetLength(0); i++)
                {
                    if (i == field.GetLength(0) - 1)
                    {
                        sbStartField.Append($"{startField[i].ToString()}");
                        sbField.Append($"{field[i].ToString()}");
                    }
                    else
                    {
                        sbStartField.Append($"{startField[i].ToString()},");
                        sbField.Append($"{field[i].ToString()},");
                    }
                }

                connection.Open();
                String sql = $"INSERT INTO [Game] ([GameStart], [GameEnd], [Height], [Width], [Field], [StartField]) VALUES (@gameStart, @gameEnd, @height, @width, @field, @startField)";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@gameStart", gameStart);
                    command.Parameters.AddWithValue("@gameEnd", gameEnd);
                    command.Parameters.AddWithValue("@height", height);
                    command.Parameters.AddWithValue("@width", width);
                    command.Parameters.AddWithValue("@field", sbField.ToString());
                    command.Parameters.AddWithValue("@startField", sbStartField.ToString());
                    command.ExecuteNonQuery();

                }
            }

        }

        /// <summary>
        /// Выгружает список сохраненных игр из БД
        /// </summary>
        /// <returns></returns>
        public List<GameModel> GameLoad()
        {
            List<GameModel> games = new List<GameModel>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                String sql = "SELECT * FROM Game";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var strField = reader.GetString(5).Split(',');
                            var strStartField = reader.GetString(6).Split(',');
                            bool[] startField = new bool[strField.GetLength(0)];
                            bool[] field = new bool[strStartField.GetLength(0)];
                            for (int i = 0; i < strField.GetLongLength(0); i++)
                            {
                                startField[i] = bool.Parse(strStartField[i]);
                                field[i] = bool.Parse(strField[i]);
                            }
                            games.Add(new GameModel()
                            {
                                Id = reader.GetInt32(0),
                                GameStart = reader.GetDateTime(1),
                                GameEnd = reader.GetDateTime(2),
                                Height = reader.GetInt32(3),
                                Width = reader.GetInt32(4),
                                GameField = field,
                                StartField = startField
                            });
                        }
                    }
                }
            }
            return games;
        }

        /// <summary>
        /// Удаляет игру из БД
        /// </summary>
        /// <param name="id">Идентификатор игры</param>
        public void GameDelete(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                String sql = $"DELETE FROM [Game] WHERE Id = @id";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Сохраняет текущую позицию в БД
        /// </summary>
        /// <param name="field">Поле</param>
        /// <param name="height">Высота</param>
        /// <param name="width">Ширина</param>
        /// <returns></returns>
        public int PositionSave(bool[] field, int height, int width)
        {
            if (PositionIsExists(field, height, width))
            {
                return 1;
            }
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < field.GetLength(0); i++)
                {
                    if (i == field.GetLength(0) - 1)
                    {
                        sb.Append($"{field[i].ToString()}");
                    }
                    else
                    {
                        sb.Append($"{field[i].ToString()},");
                    }
                }

                connection.Open();
                String sql = $"INSERT INTO [Position] ([Height], [Width], [Field], [SaveTime]) VALUES (@height, @width, @field, @saveTime)";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@height", height);
                    command.Parameters.AddWithValue("@width", width);
                    command.Parameters.AddWithValue("@saveTime", DateTime.Now);
                    command.Parameters.AddWithValue("@field", sb.ToString());
                    command.ExecuteNonQuery();
                }
            }
            return 0;
        }

        /// <summary>
        /// Выгружает список сохраненных позиций из БД
        /// </summary>
        /// <returns></returns>
        public List<PositionModel> PositionLoad()
        {
            List<PositionModel> positions = new List<PositionModel>();
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                String sql = "SELECT * FROM Position";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var str = reader.GetString(3).Split(',');
                            bool[] field = new bool[str.GetLength(0)];
                            for (int i = 0; i < str.GetLongLength(0); i++)
                            {
                                field[i] = bool.Parse(str[i]);
                            }
                            positions.Add(new PositionModel()
                            {
                                Id = reader.GetInt32(0),
                                Height = reader.GetInt32(1),
                                Width = reader.GetInt32(2),
                                GameField = field,
                                SaveTime = reader.GetDateTime(4)
                            });
                        }
                    }
                }
            }
            return positions;
        }

        /// <summary>
        /// Удаляет позицию из БД
        /// </summary>
        /// <param name="id">Идентификатор позиции</param>
        public void PositionDelete(int id)
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                String sql = $"DELETE FROM [Position] WHERE Id = @id";

                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    command.Parameters.AddWithValue("@id", id);
                    command.ExecuteNonQuery();
                }
            }
        }

        /// <summary>
        /// Проверяет существует ли сохраняемая позиция в БД
        /// </summary>
        /// <param name="field">Игровое поле</param>
        /// <param name="height">Высота</param>
        /// <param name="width">Ширина</param>
        /// <returns></returns>
        private bool PositionIsExists(bool[] field, int height, int width)
        {
            bool isExists = false;
            List<PositionModel> positions = PositionLoad();
            foreach (var p in positions)
            {
                if ((height != p.Height) || (width != p.Width))
                {
                    positions.Remove(p);
                }
            }
            foreach (var p in positions)
            {
                isExists = true;
                for (int i = 0; i < p.GameField.GetLength(0); i++)
                {
                    if (p.GameField[i] != field[i])
                    {
                        isExists = false;
                        break;
                    }
                }
            }
            return isExists;
        }
    }
}
