﻿using System;

namespace LifeGameServer
{
    /// <summary>
    /// Модель записи состояния игры в БД
    /// </summary>
    public class GameModel
    {
        public int Id { get; set; }
        public DateTime GameStart { get; set; }
        public DateTime GameEnd { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public bool[] GameField { get; set; }
        public bool[] StartField { get; set; }
    }
}
